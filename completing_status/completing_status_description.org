* Completing status
Completing status (writing-up)

If your PGR is six months or less away from submitting their thesis for examination and is no longer using specialist University equipment (such as labs), they may be eligible to transfer to completing status. Completing status is the period in which the PGR is writing-up the final draft of their thesis and offers a reduced fee of £620 p.a.


PGRs can request to transfer to completing status by submitting the online form within their Surrey Self-Service Research Programme record, under the Requests tab. They must also submit the exam entry form request at the same time so that you, as their supervisor, can select the team of examiners. The Admissions, Progression and Examination Sub-committee (APESC) will not be able to approve the completing status request until the exam entry form is complete.


PGRs can transfer to completing status as full-time or part-time. They will continue to receive full supervision whilst on completing status and must continue to log their monthly supervision meetings. PGRs will also be expected to submit progress reviews whilst on completing status in line with the timeframes advertised by the Research Degrees Office.


Transferring to completing status before submitting the thesis is not a requirement and funded PGRs may find that it is not beneficial for them to transfer to this status as it may impact on their eligibility for any remaining stipendiary payments. PGRs should check their funder’s terms and conditions before making a request to transfer to completing status if they are still within their funded period. Once funding comes to an end PGRs are charged full fees until they submit their thesis if they do not transfer to completing status.


PGRs must submit a thesis for examination before the end of the approved completing status period. If problems arise and they are unable to submit their thesis for examination by the end of the approved completing status period, they must contact the Research Degrees Office.

** Form: transferring to completing status
APESC Calendar 2021/22 Committee meetings are currently scheduled for 8 September 2021, 13 October 2021, 10 November 2021, 8 December 2021, 12 January 2022, 9 February 2022, 9 March 2022, 6 April 2022, 11 May 2022, 15 June 2022, 20 July 2022 and 14 September 2022.

At each of its monthly meetings, the Committee reviews requests that have reached the Research Degrees Office through this online system (i.e. the date submitted by PGR Director) at least two weeks prior to that meeting.

Requests received outside this period will be rolled forward for consideration at the next meeting.

You must satisfy the following criteria to be eligible for Completing Status (the reduced writing-up fee):

    Your supervisors are able to confirm that they are satisfied you are likely to complete within six months
    You are no longer using specialist resources for research

Requests for completing status WILL NOT be considered unless/until they are accompanied by:

    A detailed plan for submission within the first 6 months of completing status or by the end of registration if completing will last for less than 6 months. The plan should cover your responsibilities and your supervisors’ responsibilities during this time;
    A draft table of contents for the thesis (please use the upload function below);
    The EXAM ENTRY FORM (please submit the Exam Entry Form request if you have not already done so).

We recommend a transfer starting on the first day of the month so you do not incur that month's full tuition fee. Funded PGRs should check whether their funder will continue to pay a monthly stipend following transfer to a writing up status.

If the thesis is not submitted within the initial 6 month period of Completing Status then an updated plan for submission within the remaining period of registration will be required.

*Please upload valid supporting evidence if you wish the committee to consider a retrospective transfer to completing status*


#+DOWNLOADED: screenshot @ 2022-06-20 14:51:18
[[file:.completing_status_description-imgs/screenshot-2022-06-20_14-51-18_.png]]



** Form: exam entry
APESC Calendar 2021/22 Committee meetings are currently scheduled for 8 September 2021, 13 October 2021, 10 November 2021, 8 December 2021, 12 January 2022, 9 February 2022, 9 March 2022, 6 April 2022, 11 May 2022, 15 June 2022, 20 July 2022 and 14 September 2022.

At each of its monthly meetings, the Committee reviews requests that have reached the Research Degrees Office through this online system (i.e. the date submitted by PGR Director) at least two weeks prior to that meeting.

Requests received outside this period will be rolled forward for consideration at the next meeting.

Please fill out as much information as possible to avoid delays in processing this request. Only indicate YES to Permanent Lectureship if you hold a permanent lectureship or research position at the University of Surrey.  Indicate NO if you are not a member of staff or if you are on a short-term contract such as Research Officer/Assistant.


#+DOWNLOADED: screenshot @ 2022-06-20 14:50:56
[[file:.completing_status_description-imgs/screenshot-2022-06-20_14-50-56_.png]]

