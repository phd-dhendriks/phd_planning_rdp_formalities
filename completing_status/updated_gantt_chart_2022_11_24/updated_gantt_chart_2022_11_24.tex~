\documentclass{article}
\usepackage{pgfgantt} % https://texdoc.org/serve/pgfgantt.pdf/0

% https://mermaid.live/edit
\usepackage[margin=2cm,paperheight=12in,paperwidth=12in,]{geometry}

%\def\mystartdate{2022-38}%starting date of the calendar
%\def\myenddate{2023-09}%ending date of the calendar

\begin{document}
\section{Detailed plan completing stage}
\label{cha:deta-plan-compl}
In this document I provide a description of the broad tasks that I will work on in the completing stage, including a Gantt-chart. The main objectives are the chapters in the thesis, which will depend on finihsing the projects, writing up everything and going through the feedback process. 

\begin{center}
  {\Large \bf Gantt chart completing stage David Hendriks}
  \vspace{-0.2cm}
\end{center}

\begin{center}
  \begin{ganttchart}[
    vgrid,
    hgrid,
    %
    today=4,
    today offset=.5,
    today label=Current Week,
    today rule/.style={draw=red, thin},
    %
    x unit=0.35cm,
    y unit title=0.4cm,
    y unit chart=0.5cm,
    %
    title/.append style={shape=rectangle, fill=black!10},
    title label font = \scriptsize\color{black!80},
    title height=1,
    %
    bar/.append style={draw=black!80,fill=black!30},
    bar height=.5,
    bar label font=\footnotesize\color{black!50}
]{1}{27}

%% Define the title bars
% Years 
\gantttitle{2022}{18}
\gantttitle{2023}{9}\\
% weeks
\gantttitlelist{35,...,52}{1}\gantttitlelist{1,...,9}{1}\\
\gantttitlelist{1,...,27}{1}\\

%%%%%%%%%%%%%%%%%
% RLOF chapter
\ganttgroup{Chapter 2: binary interactions}{3}{15}\\

%%%%%%%%%%%%%%%%%
% Ballistic paper
\ganttbar[name=ballistic_paper_1]{Finish ballistic project}{3}{6}\\
\ganttbar[name=ballistic_paper_2]{Write ballistic paper}{4}{6}\\
\ganttbar[name=ballistic_paper_3]{wait on feedback ballistic paper}{7}{7}\\
\ganttbar[name=ballistic_paper_4]{process feedback ballistic paper}{8}{9}\\
\ganttbar[name=ballistic_paper_5]{Write ballistic paper into thesis and expand}{8}{10}\\

\ganttlink{ballistic_paper_1}{ballistic_paper_3}
\ganttlink{ballistic_paper_2}{ballistic_paper_3}
\ganttlink{ballistic_paper_3}{ballistic_paper_4}
\ganttlink{ballistic_paper_4}{ballistic_paper_5}

% RLOF paper
\ganttbar[name=rlof_paper_1]{Finish RLOF project}{3}{6}\\
\ganttbar[name=rlof_paper_2]{Write RLOF paper}{4}{6}\\
\ganttbar[name=rlof_paper_3]{wait on feedback RLOF paper}{7}{7}\\
\ganttbar[name=rlof_paper_4]{Process feedback RLOF paper}{8}{9}\\
\ganttbar[name=rlof_paper_5]{write RLOF paper Into thesis and expand}{8}{10}\\

\ganttlink{rlof_paper_1}{rlof_paper_3}
\ganttlink{rlof_paper_2}{rlof_paper_3}
\ganttlink{rlof_paper_1}{rlof_paper_3}
\ganttlink{rlof_paper_3}{rlof_paper_4}
\ganttlink{rlof_paper_4}{rlof_paper_5}

% Combine into thesis
\ganttbar[name=rlof_chapter_1]{Write chapter introduction and align with projects}{4}{12}\\
\ganttbar[name=rlof_chapter_3]{Clean and combine entire chapter}{12}{13}\\
\ganttbar[name=rlof_chapter_4]{Wait on feedback chapter}{13}{13}\\
\ganttbar[name=rlof_chapter_5]{process feedback chapter}{14}{15}\\

% links
\ganttlink{ballistic_paper_5}{rlof_chapter_3}
\ganttlink{rlof_paper_5}{rlof_chapter_3}
\ganttlink{rlof_chapter_3}{rlof_chapter_4}
\ganttlink{rlof_chapter_4}{rlof_chapter_5}

%%%%%%%%%%%%%%%%%
% GW project
\ganttgroup{Chapter 3: Gravitational waves}{1}{27}\\

% GW project
\ganttbar[name=gw_paper_1]{Finish GW project}{3}{6}\\
\ganttbar[name=gw_paper_2]{Finish GW paper}{4}{7}\\
\ganttbar[name=gw_paper_3]{wait on feedback GW paper}{7}{7}\\
\ganttbar[name=gw_paper_4]{process feedback GW paper}{8}{9}\\

\ganttlink{gw_paper_1}{gw_paper_3}
\ganttlink{gw_paper_2}{gw_paper_3}
\ganttlink{gw_paper_3}{gw_paper_4}

% combine into thesis
\ganttbar[name=gw_chapter_1]{Write introduction GW chapter: GW sources, binary channels and effects of binary physics}{3}{11}\\
\ganttbar[name=gw_chapter_2]{Write method GW chapter: convolution method and pop-synth}{3}{11}\\
\ganttbar[name=gw_chapter_3]{Include content of GW paper into thesis and clean}{10}{11}\\
\ganttbar[name=gw_chapter_4]{Wait on feedback chapter}{12}{12}\\
\ganttbar[name=gw_chapter_5]{process feedback chapter}{13}{14}\\

\ganttlink{gw_chapter_1}{gw_chapter_4}
\ganttlink{gw_chapter_2}{gw_chapter_4}
\ganttlink{gw_chapter_3}{gw_chapter_4}

%%%%%%%%%%%%%%%%%
% Introduction
\ganttgroup[name=intro]{Chapter: Introduction}{16}{22}\\
\ganttbar[name=intro_1]{Write introduction}{16}{20}\\
\ganttbar[name=intro_2]{Wait on feedback introduction}{20}{21}\\
\ganttbar[name=intro_3]{Process feedback introduction}{21}{22}\\

\ganttlink{intro_1}{intro_2}
\ganttlink{intro_2}{intro_3}

%%%%%%%%%%%%%%%%%
% Appendix
\ganttgroup[name=appendix]{Appendix chapter}{20}{22}\\
\ganttbar[name=apendix_1]{Include JOSS article in appendix}{20}{22}\\
\ganttbar[name=apendix_1]{Include other code-writeups in appendix}{20}{22}\\

%%%%%%%%%%%%%%%%%
% Thesis
\ganttgroup[name=thesis]{Thesis}{22}{27}\\
\ganttlink{intro}{thesis}
\ganttlink{appendix}{thesis}

\ganttbar[name=thesis_task_1]{Combine chapters}{22}{22}\\
\ganttbar[name=thesis_task_2]{Feedback on combined thesis}{23}{26}\\
\ganttbar[name=thesis_task_3]{process feedback}{26}{27}\\
\ganttmilestone[name=milestone_thesis,milestone/.append style={fill=red!50},milestone label font=\footnotesize\color{black!50}]{D1.1 Hand-in thesis}{27}\\

\ganttlink{thesis_task_1}{thesis_task_2}
\ganttlink{thesis_task_2}{thesis_task_3}
\ganttlink{thesis_task_3}{milestone_thesis}

\end{ganttchart}
\end{center}

\newpage
The tasks that are listed in the gantt chart are explained here more. I am adding time estimates, that for some of the projects start counting from today (\today{}) or from the finish of the task it relies on.
\begin{enumerate}
\item Chapter mass transfer
  \begin{enumerate}
    % ballistic
  \item Finish ballistic project: I still need to wrap up the ballistic integrations and make sure that whatever we do is valid. Then we need to run several high-resolution simulations of both the exploration and of the ballistic integrations, and then we can write up those results. Estimated time: 3 weeks.
  \item Write ballistic paper: i already have written about 6 pages, but there needs to be some interpretation, more results, more conclusion and discussion and intro and processing of the already existent feedback. Estimate: 2 Weeks.
  \item Wait on feedback ballistic paper: After having sent rob the updated version I need to wait a bit until he read it. Estimated time: 1 week
  \item Process feedback ballistic paper: processing the feedback, which will probably constist of a lot of writing feedback.
  \item Write into thesis and expand: here we take the methods, results and conclusions and write those into the thesis, so that it can serve as the segue for the next project
    % rlof
  \item Finish the RLOF project: here i still need to run things with a different disk ML assumptions and see how that affects the orbital evolution of a cluster of stars. I have several ideas for implementations of some more realistic ML routines.
  \item Write RLOF paper: there is alreayd a lot written, but much of it is going to change. Rob has sent updates and feedback already, and i will fix those and write the paper anew
  \item Wait on feedback: well yes wait on feedback
  \item process feedback: process the feedback of Rob.
  \item Write into thesis: the results and the intro method etc need to be placed into the chapter, and combined with what is written in the ballistic integration paper. This will require editing and writing.
    % thesis chapter
  \item Write chapter introduction and make sure it aligns with the two sub-projects. Write additional method or whatever is necwssary.
  \item Clean entire chapter: Clean the entire chapter and go over it to edit it and clean structure wise
  \item Wait on feedback chapter: send to rob and wait on feedback
  \item process feedback chapter: process the feedback that rob sent. 
  \end{enumerate}
  
\item Chapter grav waves: the GW observations are sensitive to binary evolution and such then are a good way to look at this from another perspective.
  \begin{enumerate}
  \item Finish GW project: Not all the simulations are fully finished yet, and I am going through the first serious round of feedback for the paper
  \item finish GW paper: with the feedback and the results of the project, I'll write up the last bits of the paper (advanced already)
  \item wait on feedback GW paper: Send back the paper to authors and have them read and give feedback again.
  \item Process feedback GW paper: I need some time to process the feedback again. 
  \item Write intro GW chapter: I will take the intro of the paper and flesh it out a bit, introduce some concepts in more detail and make it a good introduction to a chapter of the thesis
  \item Write method GW chapter: The method section that is not already covered in the earlier part (i.e pop-synth) can be fleshed out and written down. its the convolution and the extra relevant parts from pop-synth
  \item Include content of paper and clean: Now the results, discussion conclusion etc can be added to the chapter. Then polishing the chapter and making sure it fits with the introduction.
  \item Wiat on feedback: Will have to wait a bit on feedback
  \item Process feedback chapter: Will have to process the feedback of the chapter. 
  \end{enumerate}
  
\item chapter introduction
  \begin{enumerate}
  \item Write introduction: After the other 2 chapters are finished i need to write the introduction to bind them together. Estimated time: 2 weeks
  \item Wait in feedback introduction: Sending to Rob and waiting on the feedback. Estimated time 1 week
  \item Process feedback: Process the comments of Rob. Estimated time: 2 weeks.
  \end{enumerate}
  
\item Chapter appendix
  \begin{enumerate}
  \item Include JOSS article in the appendix: the JOSS paper that i wrote can go into the thesis appendix or additional material part.
  \item Include more things in the appendix: If I make another JOSS article on the ballistic integrator or the convolution code, then I can add that to the appendix too.  
  \end{enumerate}
  
\item Thesis combined
  \begin{enumerate}
  \item Combine chapters: i will write the chapters standalone, but they have to be combined into one object and have to be checked that everything fits with eachtoher, and is processed well in the main document. Est. time: 3 days.
  \item Feedback on combined thesis: I will send the thesis to rob, as a whole, for him to look at and give more comments, as i can imagine there will be some overlap between chpter parts. Est. time: 1 week
  \item Process feedback: processing the feedback of rob. Est. time: 2 weeks
  \item Hand-in Thesis: Handing in of the thesis. Speaks for itself i think. Est. time: 1 day
  \end{enumerate}
\end{enumerate}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:

