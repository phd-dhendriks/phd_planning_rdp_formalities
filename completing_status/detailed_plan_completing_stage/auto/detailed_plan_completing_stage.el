(TeX-add-style-hook
 "detailed_plan_completing_stage"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("geometry" "margin=2cm" "paperheight=12in" "paperwidth=12in" "")))
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art10"
    "pgfgantt"
    "geometry")
   (LaTeX-add-labels
    "cha:deta-plan-compl"))
 :latex)

