(TeX-add-style-hook
 "draft_table_of_contents"
 (lambda ()
   (TeX-run-style-hooks
    "latex2e"
    "report"
    "rep10")
   (LaTeX-add-labels
    "chap:draft-table-contents"))
 :latex)

