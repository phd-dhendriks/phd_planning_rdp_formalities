"""
Planning.
TODO: use some library to display the chart ? https://plotly.com/python/gantt/
"""

import datetime

DATE_FORMAT = "%Y-%m-%d"

from python_scripts.gantt_stuff import *

# Hard deadline
VIVA = Milestone(name='VIVA examination', date="2023-09-14")

# leave one month for reading etc
thesis_submission = Milestone(name='thesis submission', date=VIVA.date-pl(28))

# introduction chapter
write_introduction_chapter = Task(
    name="write introduction chapter",
    end_date=thesis_submission.date,
    reverse=True,
    length=28
)
write_conclusion_chapter = Task(
    name="write conclusion chapter",
    end_date=thesis_submission.date,
    reverse=True,
    length=28
)

#
print(VIVA)
print(thesis_submission)
print(write_introduction_chapter)
print(write_conclusion_chapter)

#
