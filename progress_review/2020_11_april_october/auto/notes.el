(TeX-add-style-hook
 "notes"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("ulem" "normalem")))
   (add-to-list 'LaTeX-verbatim-environments-local "lstlisting")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "lstinline")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "inputenc"
    "fontenc"
    "graphicx"
    "grffile"
    "longtable"
    "wrapfig"
    "rotating"
    "ulem"
    "amsmath"
    "textcomp"
    "amssymb"
    "capt-of"
    "hyperref"
    "color"
    "listings")
   (LaTeX-add-labels
    "sec:org91cddd5"
    "sec:org78f7e31"
    "sec:org7192365"
    "sec:orge606650"
    "sec:org0d4225d"
    "sec:orgedb9f31"
    "sec:orga67d80e"
    "sec:org40c9937"
    "sec:orgd49098c"
    "sec:org5059a3a"
    "sec:org9b60604"
    "sec:org32d16e8"
    "sec:org6368298"
    "sec:orge276ee1"
    "sec:org0786db9"
    "sec:org242400e"
    "sec:org2a9abfe"
    "sec:orgbba4f5e"
    "sec:org63f9ed1"
    "sec:org57e4919"
    "sec:org71551de"
    "sec:orge887598"
    "sec:org6b0f9e8"))
 :latex)

