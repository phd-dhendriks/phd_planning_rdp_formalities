\select@language {english}
\contentsline {section}{\numberline {1}Project brief}{2}{section.1}
\contentsline {section}{\numberline {2}Literature review}{4}{section.2}
\contentsline {subsection}{\numberline {2.1}Binary evolution}{4}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Disks and disk mass loss}{6}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Observational evidence and modelling efforts}{7}{subsection.2.3}
\contentsline {section}{\numberline {3}Progress so far}{10}{section.3}
\contentsline {subsection}{\numberline {3.1}Literature study}{10}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Populations of mass transfering binary systems}{10}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Disk modelling}{11}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Particle trajectories}{12}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Stellar spin up}{13}{subsection.3.5}
\contentsline {section}{\numberline {4}Published work}{15}{section.4}
\contentsline {section}{\numberline {5}Future plans}{16}{section.5}
\contentsline {subsection}{\numberline {5.1}1-d disk evolution}{16}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Detailed evolution simulations and Comparison with observations}{16}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Population}{17}{subsection.5.3}
\contentsline {section}{\numberline {6}Professional development}{18}{section.6}
\contentsline {subsection}{\numberline {6.1}Supervised summer student}{18}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Aided supervision of two bachelor students}{19}{subsection.6.2}
\contentsline {subsection}{\numberline {6.3}Refereeing a paper for MNRAS}{19}{subsection.6.3}
\contentsline {subsection}{\numberline {6.4}Outreach activities}{19}{subsection.6.4}
\contentsline {subsection}{\numberline {6.5}Winter School in November 2019}{19}{subsection.6.5}
\contentsline {subsection}{\numberline {6.6}Workshops and conferences}{20}{subsection.6.6}
\contentsline {subsection}{\numberline {6.7}Demonstrating}{20}{subsection.6.7}
\contentsline {section}{\numberline {7}todos}{21}{section.7}
\contentsline {section}{Appendix \numberline {A}Data management plan}{26}{Appendix.1.A}
\contentsline {section}{Appendix \numberline {B}Gantt-Chart}{29}{Appendix.1.B}
