\begin{thebibliography}{10}

\bibitem{de_marco_impact_2017}
O.~De~Marco and R.~G. Izzard, ``The impact of companions on stellar
  evolution,'' {\em Publications of the Astronomical Society of Australia},
  vol.~34, 2017.
\newblock arXiv: 1611.03542.

\bibitem{lubow_gas_1975}
S.~H. Lubow and F.~H. Shu, ``Gas dynamics of semidetached binaries,'' {\em The
  Astrophysical Journal}, vol.~198, pp.~383--405, June 1975.

\bibitem{flannery_gas_1975}
B.~P. Flannery, ``Gas flow in cataclysmic variable stars,'' {\em The
  Astrophysical Journal}, vol.~201, p.~661, Nov. 1975.

\bibitem{de_mink_critically_2007}
S.~E. de~Mink, O.~R. Pols, and E.~Glebbeek, ``Critically rotating stars in
  binaries - an unsolved problem -,'' {\em AIP Conference Proceedings},
  pp.~321--325, 2007.
\newblock arXiv: 0709.2285.

\bibitem{packet_spin-up_1981}
W.~Packet, ``On the spin-up of the mass accreting component in a close binary
  system,'' {\em Astronomy and Astrophysics}, vol.~102, pp.~17--19, Sept. 1981.

\bibitem{langer_coupled_1998}
N.~Langer, ``Coupled mass and angular momentum loss of massive main sequence
  stars,'' {\em Astronomy and Astrophysics}, vol.~329, pp.~551--558, Jan. 1998.

\bibitem{lynden-bell_evolution_1974}
D.~Lynden-Bell and J.~E. Pringle, ``The evolution of viscous discs and the
  origin of the nebular variables.,'' {\em Monthly Notices of the Royal
  Astronomical Society}, vol.~168, pp.~603--637, Sept. 1974.

\bibitem{shakura_black_1973}
Shakura and Sunyaev, ``Black holes in binary systems. {Observational}
  appearance.,'' 1973.

\bibitem{armitage_hydrodynamics_1998}
P.~J. Armitage and M.~Livio, ``Hydrodynamics of the {Stream}-{Disk} {Impact} in
  {Interacting} {Binaries},'' {\em The Astrophysical Journal}, vol.~493,
  pp.~898--908, Feb. 1998.

\bibitem{lubow_model_1991}
S.~H. Lubow, ``A model for tidally driven eccentric instabilities in fluid
  disks,'' {\em The Astrophysical Journal}, vol.~381, pp.~259--267, Nov. 1991.

\bibitem{osaki_early_2002}
Y.~Osaki and F.~Meyer, ``Early humps in {WZ} {Sge} stars,'' {\em Astronomy \&
  Astrophysics}, vol.~383, pp.~574--579, Feb. 2002.

\bibitem{papaloizou_tidal_1977}
J.~Papaloizou and J.~Pringle, ``Tidal torques on accretion discs in close
  binary systems,'' 1977.

\bibitem{ichikawa_tidal_1994}
S.~Ichikawa and Y.~Osaki, ``Tidal torques on accretion disks in close binary
  systems,'' {\em Publications of the Astronomical Society of Japan}, vol.~46,
  pp.~621--628, Dec. 1994.

\bibitem{neustroev_voracious_2019}
V.~V. Neustroev and S.~V. Zharikov, ``Voracious vortexes in cataclysmic
  variables: {II}. {Evidence} for the expansion of accretion disc material
  beyond the accretor's {Roche}-lobe,'' {\em arXiv:1908.10867 [astro-ph]}, Aug.
  2019.
\newblock arXiv: 1908.10867.

\bibitem{bisikalo_circumstellar_2000}
D.~V. Bisikalo, P.~Harmanec, A.~A. Boyarchuk, O.~A. Kuznetsov, and P.~Hadrava,
  ``Circumstellar structures in the eclipsing binary eta {Lyr} {A}.
  {Gasdynamical} modelling confronted with observations,'' {\em Astronomy and
  Astrophysics}, vol.~353, pp.~1009--1015, Jan. 2000.

\bibitem{kley_simulations_2008}
W.~Kley, J.~C.~B. Papaloizou, and G.~I. Ogilvie, ``Simulations of eccentric
  disks in close binary systems,'' {\em Astronomy and Astrophysics}, vol.~487,
  pp.~671--687, Aug. 2008.

\bibitem{pustylnik_early_1998}
I.~Pustylnik, ``The early history of resolving the {Algol} paradox,'' {\em
  Astronomical \& Astrophysical Transactions}, vol.~15, pp.~357--362, Apr.
  1998.

\bibitem{budding_catalogue_2004}
E.~Budding, A.~Erdem, C.~Çiçek, I.~Bulut, F.~Soydugan, E.~Soydugan,
  V.~Bakış, and O.~Demircan, ``Catalogue of {Algol} type binary stars,'' {\em
  Astronomy \& Astrophysics}, vol.~417, pp.~263--268, Apr. 2004.

\bibitem{yoon_quantitative_1994}
T.~S. Yoon, R.~K. Honeycutt, R.~H. Kaitchuck, and E.~M. Schlegel,
  ``Quantitative {Spectral} {Types} for 19 {Algol} {Secondaries},'' {\em
  Publications of the Astronomical Society of the Pacific}, vol.~106, p.~239,
  Mar. 1994.

\bibitem{richards_evidence_1993}
M.~T. Richards and G.~E. Albright, ``Evidence of {Magnetic} {Activity} in
  {Short}-{Period} {Algol} {Binaries},'' {\em The Astrophysical Journal
  Supplement Series}, vol.~88, p.~199, Sept. 1993.

\bibitem{dervisoglu_evidence_2018}
A.~Dervisoglu, K.~Pavlovski, H.~Lehmann, J.~Southworth, and D.~Bewsher,
  ``Evidence for conservative mass transfer in the classical {Algol} system
  \${\textbackslash}delta\$ {Librae} from its surface carbon-to-nitrogen
  abundance ratio,'' {\em Monthly Notices of the Royal Astronomical Society},
  vol.~481, pp.~5660--5674, Dec. 2018.
\newblock arXiv: 1810.01465.

\bibitem{richards_morphologies_1999}
M.~T. Richards and G.~E. Albright, ``Morphologies of {Halpha} {Accretion}
  {Regions} in {Algol} {Binaries},'' {\em The Astrophysical Journal Supplement
  Series}, vol.~123, p.~537, Aug. 1999.

\bibitem{marsh_images_1988}
T.~R. Marsh and K.~Horne, ``Images of accretion discs - {II}. {Doppler}
  tomography.,'' {\em Monthly Notices of the Royal Astronomical Society},
  vol.~235, p.~269, Nov. 1988.

\bibitem{van_rensbergen_accretion_2016}
W.~Van~Rensbergen and J.~P. De~Greve, ``Accretion disks in {Algols}:
  progenitors and evolution,'' {\em Astronomy \& Astrophysics}, vol.~592,
  p.~A151, Aug. 2016.
\newblock arXiv: 1604.07589.

\bibitem{mardirossian_mass_1982}
F.~Mardirossian and G.~Giuricin, ``Mass {Loss} in {Algol}-{Type} {Stars}:
  {Implication} on their {Evolutionary} {Stage},'' {\em International
  Astronomical Union Colloquium}, vol.~69, pp.~187--189, 1982.

\bibitem{de_mink_efficiency_2007}
S.~E. de~Mink, O.~R. Pols, and R.~W. Hilditch, ``Efficiency of mass transfer in
  massive close binaries, {Tests} from double-lined eclipsing binaries in the
  {SMC},'' {\em Astronomy \& Astrophysics}, vol.~467, pp.~1181--1196, June
  2007.
\newblock arXiv: astro-ph/0703480.

\bibitem{harries_ten_2003}
T.~J. Harries, R.~W. Hilditch, and I.~D. Howarth, ``Ten eclipsing binaries in
  the {Small} {Magellanic} {Cloud}: fundamental parameters and {Cloud}
  distance,'' {\em Monthly Notices of the Royal Astronomical Society},
  vol.~339, pp.~157--172, Feb. 2003.

\bibitem{hilditch_forty_2004}
R.~W. Hilditch, I.~D. Howarth, and T.~J. Harries, ``Forty eclipsing binaries in
  the {Small} {Magellanic} {Cloud}: fundamental parameters and {Cloud}
  distance,'' {\em arXiv:astro-ph/0411672}, Nov. 2004.
\newblock arXiv: astro-ph/0411672.

\bibitem{dervisoglu_spin_2010}
A.~Dervişoğlu, C.~A. Tout, and C.~İbanoğlu, ``Spin angular momentum
  evolution of the long-period {Algols},'' {\em Monthly Notices of the Royal
  Astronomical Society}, vol.~406, pp.~1071--1083, Aug. 2010.

\bibitem{tout_non-conservative_2012}
C.~A. Tout, ``Non-{Conservative} {Evolution} of {Binary} {Stars},'' vol.~282,
  pp.~417--424, Apr. 2012.

\bibitem{erdem_non-conservative_2014}
A.~Erdem and O.~Öztürk, ``Non-conservative mass transfers in {Algols},'' {\em
  Monthly Notices of the Royal Astronomical Society}, vol.~441, pp.~1166--1176,
  June 2014.

\bibitem{lomax_spectropolarimetry_2011}
J.~R. Lomax and J.~L. Hoffman, ``Spectropolarimetry of {Beta} {Lyrae}:
  {Constraining} the {Location} of the {Hot} {Spot} and {Jets},'' {\em Bulletin
  de la Société Royale des Sciences de Liège}, vol.~80, p.~5, 2011.

\bibitem{rensbergen_spin-up_2008}
W.~V. Rensbergen, J.~P.~D. Greve, C.~D. Loore, and N.~Mennekens, ``Spin-up and
  hot spots can drive mass out of a binary,'' {\em Astronomy \& Astrophysics},
  vol.~487, pp.~1129--1138, Sept. 2008.

\bibitem{rensbergen_rapid_2009}
W.~V. Rensbergen, J.~P. De~Greve, N.~Mennekens, and C.~De~Loore, ``Rapid {Mass}
  {Transfer} {Can} {Drive} {Mass} {Out} of a {Binary},'' vol.~404, p.~218, Aug.
  2009.

\bibitem{van_rensbergen_mass_2011}
W.~van Rensbergen, J.~P. de~Greve, N.~Mennekens, K.~Jansen, and C.~de~Loore,
  ``Mass loss out of close binaries. {The} formation of {Algol}-type systems,
  completed with case {B} {RLOF},'' {\em Astronomy and Astrophysics}, vol.~528,
  p.~A16, Apr. 2011.

\bibitem{deschamps_critically-rotating_2013}
R.~Deschamps, L.~Siess, P.~J. Davis, and A.~Jorissen, ``Critically-rotating
  accretors and non-conservative evolution in {Algols},'' {\em Astronomy \&
  Astrophysics}, vol.~557, p.~A40, Sept. 2013.

\bibitem{deschamps_non-conservative_2015}
R.~Deschamps, K.~Braun, A.~Jorissen, L.~Siess, M.~Baes, and P.~Camps,
  ``Non-conservative evolution in {Algols}: where is the matter?,'' {\em
  Astronomy \& Astrophysics}, vol.~577, p.~A55, May 2015.

\bibitem{izzard_new_2004}
R.~G. Izzard, C.~A. Tout, A.~I. Karakas, and O.~R. Pols, ``A new synthetic
  model for asymptotic giant branch stars,'' {\em Monthly Notices of the Royal
  Astronomical Society}, vol.~350, pp.~407--426, May 2004.

\bibitem{izzard_population_2006}
R.~G. Izzard, L.~M. Dray, A.~I. Karakas, M.~Lugaro, and C.~A. Tout,
  ``Population nucleosynthesis in single and binary stars - {I}. {Model},''
  {\em Astronomy \& Astrophysics}, vol.~460, pp.~565--572, Dec. 2006.

\bibitem{izzard_population_2009}
R.~G. Izzard, E.~Glebbeek, R.~J. Stancliffe, and O.~R. Pols, ``Population
  synthesis of binary carbon-enhanced metal-poor stars,'' {\em Astronomy \&
  Astrophysics}, vol.~508, pp.~1359--1374, Dec. 2009.

\bibitem{izzard_population_2018}
R.~G. Izzard and G.~M. Halabi, ``Population synthesis of binary stars,'' Aug.
  2018.

\bibitem{hurley_evolution_2002}
J.~R. Hurley, C.~A. Tout, and O.~R. Pols, ``Evolution of binary stars and the
  effect of tides on binary populations,'' {\em Monthly Notices of the Royal
  Astronomical Society}, vol.~329, pp.~897--928, Feb. 2002.

\bibitem{kroupa_variation_2001}
P.~Kroupa, ``On the variation of the initial mass function,'' {\em Monthly
  Notices of the Royal Astronomical Society}, vol.~322, pp.~231--246, Apr.
  2001.

\bibitem{sana_binary_2012}
H.~Sana, S.~E. de~Mink, A.~de~Koter, N.~Langer, C.~J. Evans, M.~Gieles,
  E.~Gosset, R.~G. Izzard, J.-B. Le~Bouquin, and F.~R.~N. Schneider, ``Binary
  {Interaction} {Dominates} the {Evolution} of {Massive} {Stars},'' {\em
  Science}, vol.~337, p.~444, July 2012.

\bibitem{duquennoy_multiplicity_1991}
A.~Duquennoy and M.~Mayor, ``Multiplicity among solar-type stars in the solar
  neighbourhood. {II} - {Distribution} of the orbital elements in an unbiased
  sample,'' {\em Astronomy and Astrophysics}, vol.~248, pp.~485--524, Aug.
  1991.

\bibitem{krumholz_vader_2014}
M.~R. Krumholz and J.~C. Forbes, ``{VADER}: {A} {Flexible}, {Robust},
  {Open}-{Source} {Code} for {Simulating} {Viscous} {Thin} {Accretion}
  {Disks},'' {\em arXiv:1406.6691 [astro-ph]}, June 2014.
\newblock arXiv: 1406.6691.

\bibitem{hubova_kinematics_2019}
D.~Hubová and O.~Pejcha, ``Kinematics of mass-loss from the outer {Lagrange}
  point {L}2,'' {\em Monthly Notices of the Royal Astronomical Society},
  vol.~489, pp.~891--899, Oct. 2019.

\bibitem{ud-doula_angular_2008}
A.~ud~Doula, S.~P. Owocki, and R.~H.~D. Townsend, ``Angular {Momentum} {Loss}
  and {Stellar} spin-down in {Magnetic} {Massive} {Stars},'' {\em Proceedings
  of the International Astronomical Union}, vol.~4, pp.~423--424, Nov. 2008.
\newblock arXiv: 0812.2836.

\bibitem{paxton_modules_2011}
B.~Paxton, L.~Bildsten, A.~Dotter, F.~Herwig, P.~Lesaffre, and F.~Timmes,
  ``Modules for {Experiments} in {Stellar} {Astrophysics} ({MESA}),'' {\em The
  Astrophysical Journal Supplement Series}, vol.~192, p.~3, Jan. 2011.

\bibitem{paxton_modules_2013}
B.~Paxton, M.~Cantiello, P.~Arras, L.~Bildsten, E.~F. Brown, A.~Dotter,
  C.~Mankovich, M.~H. Montgomery, D.~Stello, F.~X. Timmes, and R.~Townsend,
  ``Modules for {Experiments} in {Stellar} {Astrophysics} ({MESA}): {Planets},
  {Oscillations}, {Rotation}, and {Massive} {Stars},'' {\em The Astrophysical
  Journal Supplement Series}, vol.~208, p.~4, Sept. 2013.

\bibitem{paxton_modules_2015}
B.~Paxton, P.~Marchant, J.~Schwab, E.~B. Bauer, L.~Bildsten, M.~Cantiello,
  L.~Dessart, R.~Farmer, H.~Hu, N.~Langer, R.~H.~D. Townsend, D.~M. Townsley,
  and F.~X. Timmes, ``Modules for {Experiments} in {Stellar} {Astrophysics}
  ({MESA}): {Binaries}, {Pulsations}, and {Explosions},'' {\em The
  Astrophysical Journal Supplement Series}, vol.~220, p.~15, Sept. 2015.

\bibitem{paxton_modules_2018}
B.~Paxton, J.~Schwab, E.~B. Bauer, L.~Bildsten, S.~Blinnikov, P.~Duffell,
  R.~Farmer, J.~A. Goldberg, P.~Marchant, E.~Sorokina, A.~Thoul, R.~H.~D.
  Townsend, and F.~X. Timmes, ``Modules for {Experiments} in {Stellar}
  {Astrophysics} ({MESA}): {Convective} {Boundaries}, {Element} {Diffusion},
  and {Massive} {Star} {Explosions},'' {\em The Astrophysical Journal
  Supplement Series}, vol.~234, p.~34, Feb. 2018.

\bibitem{paxton_modules_2019}
B.~Paxton, R.~Smolec, A.~Gautschy, L.~Bildsten, M.~Cantiello, A.~Dotter,
  R.~Farmer, J.~A. Goldberg, A.~S. Jermyn, S.~M. Kanbur, P.~Marchant,
  J.~Schwab, A.~Thoul, R.~H.~D. Townsend, W.~M. Wolf, M.~Zhang, and F.~X.
  Timmes, ``Modules for {Experiments} in {Stellar} {Astrophysics} ({MESA}):
  {Pulsating} {Variable} {Stars}, {Rotation}, {Convective} {Boundaries}, and
  {Energy} {Conservation},'' {\em arXiv:1903.01426 [astro-ph]}, Mar. 2019.
\newblock arXiv: 1903.01426.

\bibitem{matson_radial_2017}
R.~A. Matson, D.~R. Gies, Z.~Guo, and S.~J. Williams, ``Radial {Velocities} of
  41 {Kepler} {Eclipsing} {Binaries},'' {\em The Astronomical Journal},
  vol.~154, p.~216, Nov. 2017.
\newblock arXiv: 1710.03840.

\bibitem{song_close_2018}
H.~F. Song, G.~Meynet, A.~Maeder, S.~Ekström, P.~Eggenberger, C.~Georgy,
  Y.~Qin, T.~Fragos, M.~Soerensen, F.~Barblan, and G.~A. Wade, ``Close binary
  evolution: {II}. {Impact} of tides, wind magnetic braking, and internal
  angular momentum transport,'' {\em Astronomy \& Astrophysics}, vol.~609,
  p.~A3, Jan. 2018.

\end{thebibliography}
