
Begin forwarded message:

Date: Wed, 19 May 2021 13:47:29 +0100
From: "Sellin, Paul Prof (Physics)" <p.sellin@surrey.ac.uk>
To: phacademics <phacademics@surrey.ac.uk>
Subject: Urgent: Non-Covid Extensions for Funded PGR Students


Dear All,

Apologies for the mass email but this is the easiest way to reach everyone quickly.

The faculty has some additional funds available to support students with a small stipend extension, if their funding is due to run out before 30 Sept 2021. This would apply to cases where the student has experienced delays which are not primarily due to Covid. If you think you have a student in this category, please read the details below and contact me if you need more information.

Supervisors can also apply for these funds to cover small pieces of equipment or facility access for their PGR student, if this can be shown to help mitigate recent delays in the project. Any such funds would also have to be spent by this September (at the level of £2k - £4k). Please contact me if you want to find out more about this.

For students who are experiencing delays primarily due to Covid disruption, the correct route for additional funding support continues to be through the separate Covid disruption fund.

Thanks

Paul

Subject: Non-Covid Extensions for Funded PGR Students

Dear PGRDs and Department Administrators,

As discussed at the FRDC on Thursday 13 May there are some unspent funds to be spent before the end of September 2021, which will be used to support current PGR students towards the completion of their degrees. 

One of the opportunities identified is for funded students whose funding expires before September 2021 and where their progress is delayed for reasons not primarily due to Covid (so they are not eligible for the Disruption Fund). In cases where such students have been funded by the DTP training grant, we have been able to offer up to 6 months of extended funding (fees and stipends as applicable). The present opportunity extends this support to all funded students (i.e. where stipends are paid via the University of Surrey, or where the university provides a fees-only scholarship), and the same criteria for support will be used.

The eligibility requirements for the Disruption Fund (supporting Covid-related disruption) are listed here https://www.surrey.ac.uk/coronavirus/students/postgraduate-research-students

and it may be worth reminding students that acceptable disrupting factors are not limited to loss of access to facilities, but also include additional caring responsibilities, physical and mental wellbeing, and other factors described at the page linked above.

The conditions for support for non-Covid disruption are:

(1)      The PGR is registered with the University for the period covered by the extension.

(2)      The student is/was funded (i.e. receives their stipend through the University, whatever the original source of funds).

(3)      The student’s progress has been disrupted by circumstances beyond their control which are NOT primarily Covid-related. Applications for support due to Covid-related disruption should be made instead to the Disruption Fund. Separate applications for Covid- and non-Covid- related disruptions are permitted.

(4)      The student has not already received a non-Covid funding extension.  

(5)      The maximum duration of extension is 6 months and the latest end-date is September 30, 2021. 

(6)      Where a stipend is paid, it will be at the current UKRI rate.

(7)      Applications should be made to j.allam@surrey.ac.uk and accompanied by a brief case (see below), and will be assessed by a panel of PGRDs.

(8)      Once approved, Student Finance will be instructed to pay the extension from the appropriate code.

The (brief, < 1 side of A4) case for support should address:
How the student’s progress was disrupted and the extend to which this was beyond the student’s control.
Any previous steps taken to mitigate the disruption, and their impact on the student’s completion.
Why the student’s completion of their PhD is at risk without the extension of funding.
A brief plan for completion within the funded extension period.
Whether any application to the Disruption Fund has been made, or an extension granted.
A brief supporting statement from the supervisor.
